from goto import with_goto

"""
Conjunto de macros para operações com variaveis
"""
@with_goto
def copy_var_value_bug(origim, destiny):
    """Copiar o valor de uma variavel para outra, o bug é o caso de origim ser 0, 
    visto que ele se torna -1 no if e entra em loop infinito

    Args:
        origim (list): lista com a variavel com valor original, n deve ser 0
        destiny (list): Por convenção, o valor deve ser 0, lista com a variavel de destino do valor
    """


    label .A

    origim[0] = origim[0] - 1

    destiny[0] = destiny[0] + 1

    # caso seja 0, o valor foi totalmente transferido
    if origim[0] != 0: 
        goto .A

@with_goto
def copy_var_value(origim, destiny):
    """Copiar o valor de uma variavel para outra, sem bug

    Args:
        origim (list): lista com a variavel com valor original
        destiny (list): Por convenção, o valor deve ser 0, lista com a variavel de destino do valor
    """
    
    #variavel auxiliar para determinar a saida do programa
    flag = [0]


    label .A
    #verificação inicial dos valores, caso a origem ainda n esteja zerada, os valores devem ser tranferidos
    # caso a origim seja 0, a flag é ativada e o alg termina
    if origim[0] != 0:
        goto .B

    flag[0] = flag[0] + 1

    # saida do programa
    if flag[0] != 0:
        goto .E 

    label .B
    # tranferencia normal de valores e ativamento da flag
    origim[0] = origim[0] - 1
    destiny[0] = destiny[0] + 1

    flag[0] = flag[0] + 1

    # parece ser redundante, mas isso leva, obrigatoriamente, o alg para as verificações iniciais dos valores
    if flag[0] != 0:
        goto .A  
    
    label .E 