from goto import with_goto


@with_goto
def triplo(value):
	"""
	Computa o triplo de um valor
	Args:
		value: Valor a ser triplicado

	Returns:
		int: o valor triplicado

	"""
	# controlador do loop
	loop = 3

	# acumulador da multiplicação
	acul = 0

	# while loop != 0
	label .loop

	# acaumulação da soma
	acul = acul + value

	loop = loop - 1

	# break do loop
	if loop != 0:
		goto .loop

	# returno do valor
	return acul


@with_goto
def soma(val1, val2):
	"""
	Computa a soma entre dois números
	Args:
		val1: valor 1
		val2: valor 2

	Returns:
		int: a soma dos dois números
	"""

	label .loop

	# while val1 != 0
	if val1 != 0:
		val2 = val2 + 1

		val1 = val1 - 1

		goto .loop

	# val 2 ja foi acrescido do val 1
	return val2


@with_goto
def triplo_sem_macro(value):
	"""
	Computa o triplo sem usar macros
	Args:
		value: valor para ser triplicado

	Returns:
		int: valor triplicado
	"""

	# controlador do loop
	loop = 3

	# acumulador da multiplicação
	acul = 0

	# while loop != 0
	label .loop

	# acul = acul + value
	acul = soma(value, acul)

	loop = loop - 1

	# break do loop
	if loop != 0:
		goto .loop

	# return value
	return acul


@with_goto
def maior_valor(val1, val2):
	"""
	Descobre o maior valor entre val1 e val2 (val1 <= val2)
	Args:
		val1: valor 1
		val2: valor 2

	Returns:
		0: Se o valor 1 for maior que valor 2
		1: Se o valor 2 for maior que o valor 1

	"""

	label .loop

	# flags para saber se os valores zeraram
	flag_val1 = 0
	flag_val2 = 0

	if val1 != 0:
		flag_val1 = 1

	if val2 != 0:
		flag_val2 = 1

	if flag_val1 != 0:

		if flag_val2 != 0:

			# os dois valores ainda não zeraram
			val1 = val1 - 1

			val2 = val2 - 1

			goto .loop

		# se chegou aq, flag_val2 é 0, ou seja, val1 é maior
		return 0

	# se chegou aq, flag_val1 é 0, ou seja, val 2 é maior
	return 1


def igual(val1, val2):
	"""
	Descobre se os valores são iguais
	Args:
		val1: valor 1
		val2: valor 2

	Returns:
		1: se os valores são iguais
		0: se os valores são diferentes

	"""

	flag1_pos = maior_valor(val1, val2)

	flag2_pos = maior_valor(val2, val1)

	if flag1_pos != 0:

		if flag2_pos != 0:
			# se val1 <= val2 e val2 <= val1 é verdade, então eles são iguais
			return 1

	# eles são diferentes
	return 0


@with_goto
def par_impar(value):
	"""
	Computa se o valor é par ou impar
	Args:
		value: valor a ser analisado

	Returns:
		0: ele é par
		1: ele é impar

	"""
	# numero por pode ser representado por um múltiplo de 2
	par_case = 0

	# numero impar pode ser representado por um multiplo de 2 mais 1
	impar_case = 1

	label .loop

	par_case = par_case + 2

	impar_case = impar_case + 2

	# flags para detectar qual do casos representou o numero
	flag_par = igual(par_case, value)
	flag_impar = igual(impar_case, value)

	if flag_par != 0:
		return 0

	if flag_impar != 0:
		return 1

	goto .loop


@with_goto
def quadrado(value):
	"""
	Computa o valor elevado ao quadrado
	Args:
		value: valor que será elevado ao quadrado

	Returns:
		int: valor na potencia 2
	"""

	# controlador do loop, note que é a função do triplo com modificação no loop
	loop = value

	# acumulador da multiplicação
	acul = 0

	# while loop != 0
	label.loop

	# caumulação da soma
	acul = acul + value

	loop = loop - 1

	# break do loop
	if loop != 0:
		goto.loop

	# return value
	return acul


@with_goto
def questao_6(value):
	"""
	Computa o valor maximo de n tal que n² <= value
	Args:
		value: teto de n

	Returns:
		int: n máximo
	"""
	n = 0

	label .loop

	passo_anterior = n

	n = n + 1

	potencia = quadrado(n)

	max_value = maior_valor(value, potencia)

	if max_value != 0:
		return passo_anterior

	goto .loop


@with_goto
def mdc(val1, val2):
	"""
	Computa o mdc entre val1 e val2
	Args:
		val1: valor 1
		val2: valor 2

	Returns:
		int: o maior divisor comum de val1 e val2
	"""

	# etapa para padronizar as operações
	max_ = maior_valor(val1, val2)
	if max_ != 0:
		aux = val1
		val1 = val2
		val2 = aux

	# while val1 != val2
	label .loop

	# criterio de parada do loop, o mdc propriamente dito
	igual_ = igual(val1, val2)

	if igual_ != 0:
		return val1

	# verificação para, caso val 1 e val 2 não sejam multiplos comuns
	max_ = maior_valor(val1, val2)
	if max_ != 0:
		return val1

	# subtraindo o menor do maior
	val1 = val1 - val2

	goto .loop


